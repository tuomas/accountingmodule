namespace AccountingModule.Model.Ledgers
{
    public class AccountTotal
    {
        public int Count { get; }
        public decimal Total { get; }

        public AccountTotal(int count, decimal total)
        {
            Count = count;
            Total = total;
        }
    }
}