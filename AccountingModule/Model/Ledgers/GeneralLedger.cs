using System;
using AccountingModule.Model.BusinessTransactions;

namespace AccountingModule.Model.Ledgers
{
    public class GeneralLedger : Ledger
    {
        private Client _client;
        private Ledger _accountReceivables;
        private Ledger _clientPayables;
        private Ledger _supplierPayables;
        private Ledger _commissions;
        private Ledger _clientLoanReceivables;
        private Ledger _salesVatDebt;

        public GeneralLedger(Client client)
        {
            _client = client;
            _accountReceivables = new Ledger();
            _clientPayables = new Ledger();
            _supplierPayables = new Ledger();
            _commissions = new Ledger();
            _clientLoanReceivables = new Ledger();
            _salesVatDebt = new Ledger();
        }

        public void WriteJournalEntry(IBusinessTransaction businessTransaction)
        {
            if (businessTransaction.GetType() == typeof(SalesInvoicePurchase))
            {
                _accountReceivables.WriteDebit(businessTransaction);
                _clientPayables.WriteCredit(businessTransaction);
            }
            else if (businessTransaction.GetType() == typeof(PurchaseInvoiceFinancing))
            {
                _supplierPayables.WriteCredit(businessTransaction);
                _clientLoanReceivables.WriteDebit(businessTransaction);
            }
            else if (businessTransaction.GetType() == typeof(CashDiscount))
            {
                _accountReceivables.WriteCredit(businessTransaction);
                _clientPayables.WriteDebit(businessTransaction);
            }
            else if (businessTransaction.GetType() == typeof(Commission))
            {
                _clientPayables.WriteDebit(businessTransaction);
                _commissions.WriteCredit(businessTransaction);
            }
            else
            {
                throw new NotSupportedException("Business transaction type not supported");
            }
        }

        public Statement GenerateStatement()
        {
            AccountTotal commissionsCredit = _commissions.GenerateCreditAccountAggregate();

            return new Statement(
                _client.Name,
                _clientPayables.GenerateDebitAccountAggregate().Count,
                _supplierPayables.GenerateCreditAccountAggregate().Count,
                commissionsCredit.Count,
                commissionsCredit.Total,
                _clientLoanReceivables.GenerateDebitAccountAggregate().Total,
                _clientPayables.GenerateCreditAccountAggregate().Total
            );
        }
    }
}