using System.Collections.Generic;
using System.Linq;
using AccountingModule.Model.BusinessTransactions;

namespace AccountingModule.Model.Ledgers
{
    public class Ledger
    {
        private List<IBusinessTransaction> _debit;
        private List<IBusinessTransaction> _credit;

        public Ledger()
        {
            _debit = new List<IBusinessTransaction>();
            _credit = new List<IBusinessTransaction>();
        }

        public void WriteDebit(IBusinessTransaction businessTransaction)
        {
            _debit.Add(businessTransaction);
        }

        public void WriteCredit(IBusinessTransaction businessTransaction)
        {
            _credit.Add(businessTransaction);
        }

        public AccountTotal GenerateDebitAccountAggregate()
        {
            return new AccountTotal(_debit.Count, _debit.Sum(it => it.Amount));
        }

        public AccountTotal GenerateCreditAccountAggregate()
        {
            return new AccountTotal(_credit.Count, _credit.Sum(it => it.Amount));
        }
    }
}