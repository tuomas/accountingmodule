using FileHelpers;

namespace AccountingModule.Model.Ledgers
{
    
    public class Statement
    {
        public string ClientName { get; }
        public int SalesInvoicePurchaseCount { get; }
        public int FinancedPurchaseInvoicesCount { get; }
        public int CommissionsCount { get; }
        public decimal TotalCommissions { get; }
        public decimal TotalClientLoanReceivables { get; }
        public decimal TotalClientPayables { get; }

        public Statement(string clientName, int salesInvoicePurchaseCount, int financedPurchaseInvoicesCount, 
            int commissionsCount, decimal totalCommissions, decimal totalClientLoanReceivables, decimal totalClientPayables)
        {
            ClientName = clientName;
            SalesInvoicePurchaseCount = salesInvoicePurchaseCount;
            FinancedPurchaseInvoicesCount = financedPurchaseInvoicesCount;
            CommissionsCount = commissionsCount;
            TotalCommissions = totalCommissions;
            TotalClientLoanReceivables = totalClientLoanReceivables;
            TotalClientPayables = totalClientPayables;
        }
    }
}