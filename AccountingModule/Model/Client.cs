namespace AccountingModule.Model
{
    public class Client
    {
        public string Name { get; set; }
        public Country Country { get; set; }
    }
}