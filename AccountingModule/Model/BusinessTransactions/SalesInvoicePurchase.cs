using System;

namespace AccountingModule.Model.BusinessTransactions
{
    public class SalesInvoicePurchase : AbstractBusinessTransaction
    {
        public SalesInvoicePurchase()
        {
        }

        public SalesInvoicePurchase(Client client, decimal amount, Currency currency, DateTime timestamp) 
            : base(client, amount, currency, timestamp)
        {
        }
    }
}