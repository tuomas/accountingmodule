using System;

namespace AccountingModule.Model.BusinessTransactions
{
    public class CashDiscount : AbstractBusinessTransaction
    {

        public CashDiscount()
        {
        }

        public CashDiscount(Client client, decimal amount, Currency currency, DateTime timestamp) 
            : base(client, amount, currency, timestamp)
        {
        }
    }
}