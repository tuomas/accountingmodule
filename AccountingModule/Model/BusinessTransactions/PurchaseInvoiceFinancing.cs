using System;

namespace AccountingModule.Model.BusinessTransactions
{
    public class PurchaseInvoiceFinancing : AbstractBusinessTransaction
    {
        public PurchaseInvoiceFinancing()
        {
        }

        public PurchaseInvoiceFinancing(Client client, decimal amount, Currency currency, DateTime timestamp) 
            : base(client, amount, currency, timestamp)
        {
        }
    }
}