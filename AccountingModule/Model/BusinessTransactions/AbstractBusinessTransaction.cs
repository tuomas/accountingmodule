using System;

namespace AccountingModule.Model.BusinessTransactions
{
    public abstract class AbstractBusinessTransaction : IBusinessTransaction
    {
        public Client Client { get; set; }
        public string CustomerOrSupplier { get; set; }
        public decimal Amount { get; set; }
        public Currency Currency { get; set; }
        public DateTime Timestamp { get; set; }

        public AbstractBusinessTransaction()
        {
        }

        public AbstractBusinessTransaction(Client client, decimal amount, Currency currency, DateTime timestamp)
        {
            Client = client;
            Amount = amount;
            Currency = currency;
            Timestamp = timestamp;
        }
    }
}