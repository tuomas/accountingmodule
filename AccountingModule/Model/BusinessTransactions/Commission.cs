using System;

namespace AccountingModule.Model.BusinessTransactions
{
    public class Commission : AbstractBusinessTransaction
    {

        public Commission()
        {
        }

        public Commission(Client client, decimal amount, Currency currency, DateTime timestamp) 
            : base(client, amount, currency, timestamp)
        {
        }
    }
}