using System;

namespace AccountingModule.Model.BusinessTransactions
{
    public interface IBusinessTransaction
    {
        Client Client { get; set; }

        string CustomerOrSupplier { get; set; }
        decimal Amount { get; set; }
        Currency Currency { get; set; }
        DateTime Timestamp { get; set; }
                
    }
}