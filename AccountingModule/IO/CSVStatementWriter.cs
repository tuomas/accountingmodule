using System;
using System.Collections.Generic;
using System.Linq;
using AccountingModule.Model.Ledgers;
using FileHelpers;

namespace AccountingModule.IO
{
    public class CSVStatementWriter : IStatementWriter<string>
    {
        private FileHelperEngine<Output> _engine;

        public CSVStatementWriter()
        {
            _engine = new FileHelperEngine<Output>();
            _engine.HeaderText = "Client, Number of sales invoice purchases, Number of financed purchase invoices, " +
                                 "Commissions, Total Commissions (EUR), Client Loan Receivables, Client Payables";
        }
    
        public string writeStatement(ICollection<Statement> statements)
        {
            return _engine.WriteString(statements.Select(StatementToOutput));
        }

        private Output StatementToOutput(Statement statement)
        {
            Output output = new Output
            {
                ClientName = statement.ClientName,
                SalesInvoicePurchaseCount = statement.SalesInvoicePurchaseCount,
                FinancedPurchaseInvoicesCount = statement.FinancedPurchaseInvoicesCount,
                CommissionsCount = statement.CommissionsCount,
                TotalCommissions = statement.TotalCommissions,
                TotalClientLoanReceivables = statement.TotalClientLoanReceivables,
                TotalClientPayables = statement.TotalClientPayables
            };

            return output;
        }
    }
}