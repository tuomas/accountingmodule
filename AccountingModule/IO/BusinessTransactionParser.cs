using System.Collections.Generic;
using AccountingModule.Model;
using AccountingModule.Model.BusinessTransactions;

namespace AccountingModule.IO
{
    public interface IBusinessTransactionParser<T>
    {
        ICollection<IBusinessTransaction> parse(T input);
    }
}