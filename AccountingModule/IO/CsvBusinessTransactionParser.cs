using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using AccountingModule.Model;
using AccountingModule.Model.BusinessTransactions;
using AccountingModule.Repository;
using FileHelpers;

namespace AccountingModule.IO
{
    public class CsvBusinessTransactionParser : IBusinessTransactionParser<string>
    {

        private ClientRepository _repository;
        private FileHelperEngine<Input> _engine;

        public CsvBusinessTransactionParser(ClientRepository clientRepository)
        {
            _repository = clientRepository;
            _engine = new FileHelperEngine<Input>();
        }
    
        public ICollection<IBusinessTransaction> parse(string input)
        {
            var events = _engine.ReadString(input);
            return events.Select(buildBusinessTransaction).ToList();
        }

        private IBusinessTransaction buildBusinessTransaction(Input input)
        {
            IBusinessTransaction businessTransaction = null;
        
            if (IsValidInput(input))
            {
                if (input.BusinessTransaction.Equals(typeof(Commission).Name))
                {
                    businessTransaction = new Commission();
                }
                else if (input.BusinessTransaction.Equals(typeof(CashDiscount).Name))
                {
                    businessTransaction = new CashDiscount();            
                }
                else if (input.BusinessTransaction.Equals(typeof(PurchaseInvoiceFinancing).Name))
                {
                    businessTransaction = new PurchaseInvoiceFinancing();
                }
                else if (input.BusinessTransaction.Equals(typeof(SalesInvoicePurchase).Name)) 
                {
                    businessTransaction = new SalesInvoicePurchase();
                }

                businessTransaction.Client = _repository.GetClientByName(input.Name);
                businessTransaction.CustomerOrSupplier = input.CustomerSupplier;
                businessTransaction.Amount = input.Amount;
                businessTransaction.Currency = Currency.EUR;
                businessTransaction.Timestamp = input.Timestamp;

                return businessTransaction;
            }

            throw new Exception("Invalid business transaction");            
        }

        private bool IsValidInput(Input input)
        {
            return true;
        }

    }
}