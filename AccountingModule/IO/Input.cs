
using System;
using FileHelpers;

namespace AccountingModule.IO
{
    [DelimitedRecord(",")]
    public class Input
    {
        [FieldTrim(TrimMode.Right)]
        public string Name { get; set; }

        [FieldTrim(TrimMode.Right)]
        public string CustomerSupplier { get; set; }

        [FieldTrim(TrimMode.Right)]
        public string BusinessTransaction { get; set; }
        
        [FieldConverter(ConverterKind.Decimal)]
        [FieldTrim(TrimMode.Right)]
        public decimal Amount { get; set; }

        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd hh:mm:ss")]
        [FieldTrim(TrimMode.Right)]
        public DateTime Timestamp { get; set; }
    }
}