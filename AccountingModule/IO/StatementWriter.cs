using System.Collections.Generic;
using AccountingModule.Model.Ledgers;

namespace AccountingModule.IO
{
    public interface IStatementWriter<T>
    {
        T writeStatement(ICollection<Statement> statements);
    }
}