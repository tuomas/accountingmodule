using FileHelpers;

namespace AccountingModule.IO
{
    [DelimitedRecord(",")]
    public class Output
    {
        public string ClientName { get; set; }
        public int SalesInvoicePurchaseCount { get; set; }
        public int FinancedPurchaseInvoicesCount { get; set; }
        public int CommissionsCount { get; set; }
        public decimal TotalCommissions { get; set; }
        public decimal TotalClientLoanReceivables { get; set; }
        public decimal TotalClientPayables { get; set; }
        
    }
}