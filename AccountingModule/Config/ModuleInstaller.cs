using AccountingModule.IO;
using AccountingModule.Repository;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace AccountingModule.Config
{
    public class ModuleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<AccountingService>());
            container.Register(Component.For<ClientRepository>());
            container.Register(Component.For<LedgerRepository>());
            
            container.Register(Component.For<IBusinessTransactionParser<string>>()
                .ImplementedBy<CsvBusinessTransactionParser>());
            container.Register(Component.For<IStatementWriter<string>>()
                .ImplementedBy<CSVStatementWriter>());
        }
    }
}