using System.Collections.Generic;
using System.Linq;
using AccountingModule.Model.BusinessTransactions;
using AccountingModule.Model.Ledgers;
using AccountingModule.Repository;

namespace AccountingModule
{
    public class AccountingService
    {
        private ClientRepository _clientRepository;
        private LedgerRepository _ledgerRepository;

        public AccountingService(ClientRepository clientRepository, LedgerRepository ledgerRepository)
        {
            _clientRepository = clientRepository;
            _ledgerRepository = ledgerRepository;
        }

        public void ProcessBusinessTransactions(ICollection<IBusinessTransaction> businessTransactions)
        {
            foreach (var businessTransaction in businessTransactions)
            {
                ProcessBusinessTransaction(businessTransaction);
            }
        }

        public void ProcessBusinessTransaction(IBusinessTransaction businessTransaction)
        {
            GeneralLedger ledger = _ledgerRepository.GetLedgerByName(businessTransaction.Client.Name);
            ledger.WriteJournalEntry(businessTransaction);
        }


        public ICollection<Statement> GenerateStatement()
        {
            return _clientRepository.getAllClients()
                .Select(client => _ledgerRepository.GetLedgerByName(client.Name).GenerateStatement()).ToList();
        }
    }
}