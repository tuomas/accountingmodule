using System;
using System.Collections.Generic;
using AccountingModule.Model;

namespace AccountingModule.Repository
{
    public class ClientRepository
    {
        private Dictionary<string, Client> _clients;

        public ClientRepository()
        {
            _clients = new Dictionary<string, Client>();

            foreach (Client client in Clients())
            {
                _clients.Add(client.Name, client);    
            }
        }

        public ICollection<Client> getAllClients()
        {
            return _clients.Values;
        }

        public Client GetClientByName(string name)
        {
            if (!_clients.ContainsKey(name))
            {
                throw new Exception();
            }

            return _clients[name];
        }

        public static ICollection<Client> Clients()
        {
            ICollection<Client> clients = new List<Client>();

            Client client1 = new Client();
            client1.Name = "FinKing Roastery Ab";
            client1.Country = Country.FIN;
            
            Client client2 = new Client();
            client2.Name = "SweFashion Ab";
            client2.Country = Country.SWE;

            clients.Add(client1);
            clients.Add(client2);

            return clients;
        }
    }
}