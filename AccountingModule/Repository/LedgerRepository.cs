using System.Collections.Generic;
using AccountingModule.Model;
using AccountingModule.Model.Ledgers;

namespace AccountingModule.Repository
{
    public class LedgerRepository
    {
        private IDictionary<string, GeneralLedger> _ledgers;

        public LedgerRepository()
        {
            _ledgers = new Dictionary<string, GeneralLedger>();

            foreach (Client client in ClientRepository.Clients()) 
            {
                _ledgers.Add(client.Name, new GeneralLedger(client));
            }
        }

        public GeneralLedger GetLedgerByName(string clientName)
        {
            return _ledgers[clientName];
        }
    }
}