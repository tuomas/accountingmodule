using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using AccountingModule;
using AccountingModule.Config;
using AccountingModule.IO;
using AccountingModule.Model.BusinessTransactions;
using AccountingModule.Model.Ledgers;
using AccountingModule.Repository;
using Castle.Windsor;
using NUnit.Framework;

namespace Tests
{
    public class AccountingModuleTests
    {
        private IBusinessTransactionParser<string> _parser;
        private IStatementWriter<string> _writer;
        private AccountingService _accountingService; 

        [SetUp]
        public void Setup()
        {
            IWindsorContainer container = new WindsorContainer();
            container.Install(new ModuleInstaller());

            _accountingService = container.Resolve<AccountingService>();
            _parser = container.Resolve<IBusinessTransactionParser<string>>();
            _writer = container.Resolve<IStatementWriter<string>>();
        }

        [Test]
        public void TestReadAndOutputData()
        {
            string csvContent = null;
            using (StreamReader reader = new StreamReader(Assembly.GetExecutingAssembly()
                .GetManifestResourceStream("AccountingModule.Tests.input.csv")))
            {
                csvContent = reader.ReadToEnd();
            }
 
            ICollection<IBusinessTransaction> businessTransactions = _parser.parse(csvContent);
            _accountingService.ProcessBusinessTransactions(businessTransactions);

            ICollection<Statement> statements = _accountingService.GenerateStatement();
            Console.Out.WriteLine(_writer.writeStatement(statements));
        }
    }
}